#assignment-7-shanks2710
# Assignment 7
This project aims at creating Contact/PhoneBook app that provides the following features:
1) View all contacts
2) Add a new Contact/PhoneBook
3) View Details of a specific contact

# Prerequisites
This project requires npm to install plugins for its dependencies and to run commands to operate the server.
It uses webpack to build and run the application. The styling requires SCSS attributes. Additionally, the app requires Observable and fromEvent components of RxJS to 
attach and manage events.

# Built With
Visual Source Code - IDE to create project.
Browser - To run the application and view the app

# Run
Steps:
1) Clone the GitHub repo: https://github.com/neu-mis-info6150-spring-2019/assignment-7-shanks2710.git
2) Open Visual Source Code and Select File->Open Folder
3) Navigate to the respective folder and Click Select Folder
4) Go to Terminal Section
5) Run the command to initialize npm: npm init
6) Run the command to install the dependencies as mentioned in package.json file: npm install
7) Run the command to build the application using Webpack: npm run build
8) Run the command to start the application: npm run start
7) The application runs on URL: http://localhost:8080 and the index.html page is displayed in the default-browser
9) To stop the application, run command 'Ctrl+C' and type 'Y' in the Terminal

# Author
Shashank Sharma, NU-ID: 001415411

# Acknowledgments
Prof. Amuthan Arulraj and all TAs