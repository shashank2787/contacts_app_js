//Imports for RxJS
import { Observable } from 'rxjs';
import { fromEvent } from 'rxjs';

//Window-Load Event-Listener
const initApp$ = fromEvent(window, 'load')
    .subscribe((event) => {
        initializeApplication();
    });
//Method to set initialize application behavior
function initializeApplication() {
    //Defualt-View Settings
    document.getElementById('showAllContacts').style.display = 'block';
    document.getElementById('addSection').style.display = 'none';

    //Attach Event-listeners to buttons
    //Show Contacts Button
    const displayAll = document.getElementById('displayAll');
    const showContact$ = fromEvent(displayAll, 'click')
        .subscribe(() => {
            getAllContacts();
            document.getElementById('showAllContacts').style.display = 'block';
            document.getElementById('addSection').style.display = 'none';
        });

    //Add-Contact Button
    const addContact = document.getElementById('addContact');
    const displayAddContact$ = fromEvent(addContact, 'click')
        .subscribe(() => {
            document.getElementById('showAllContacts').style.display = 'none';
            document.getElementById('addSection').style.display = 'block';
        });


    //Event-Listeners for Contact-List in Show-Contact section
    const showAllContacts = document.getElementById('showAllContacts');
    const viewContact$ = fromEvent(showAllContacts, 'click')
        .subscribe((e) => {
            if (e.target && e.target.className == 'contact-item') {
                console.log('show details method called: ' + e.target.id);
                showContactDetails(e.target.id);
            }
        });


    //Add-Contact button Event-listeners
    const addBtn = document.getElementById('addBtn');
    const addContact$ = fromEvent(addBtn, 'click')
        .subscribe(() => {
            addContacts();
        });

    //Display contact-details as a Modal
    const modal = document.getElementById('myModal');

    //Add close-events for the Modal
    const closeBtn = document.getElementsByClassName("close")[0];
    const closeViewContact$ = fromEvent(closeBtn, 'click')
        .subscribe(() => {
            modal.style.display = "none";
        });

    const escViewContact$ = fromEvent(window, 'click')
        .subscribe((event) => {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        });
}

//Function to retrieve all contacts
function getAllContacts() {
    //Fetch-API call via Observables
    document.getElementById('showAllContacts').innerHTML = '';
    const contact$ = Observable.create(observer => {
        fetch('http://localhost:3000/contacts')
            .then(response => response.json())
            .then(contact => {
                observer.next(contact);
                observer.complete();
            })
            .catch(err => observer.error(err));
    });
    //Subscription to the Observable
    contact$.subscribe(contact => {
        if (contact.length > 0) {
            for (var i = 0; i < contact.length; i++) {
                console.log(contact[i]._id);
                let li = document.createElement('div');
                li.id = contact[i]._id;
                li.className = 'contact-item';
                li.innerHTML = contact[i].lastname + ', ' + contact[i].firstname + '<hr>';
                document.getElementById('showAllContacts').appendChild(li);
            }
        } else {
            document.getElementById('showAllContacts').innerHTML += '<div class="contact-item">You have no contacts. Why not add  a few?</div><hr>';
        }
    });
}


//Function to add contact
function addContacts() {
    //Get values from Form-Fields
    document.getElementById('showAllContacts').innerHTML = '';
    let fname = document.getElementById('fname').value;
    let lname = document.getElementById('lname').value;
    let phone = document.getElementById('phone').value;
    let email = document.getElementById('email').value;

    //Validations for Form-Fields
    let errorText = '';
    let valid = true;
    if (!fname || fname.replace(/ /g, '').length == 0) {
        valid = false;
        errorText = (errorText.length > 0) ? errorText += '<br>First-Name is mandatory!' : errorText = 'First-Name is mandatory!';
    }
    if (!lname || lname.replace(/ /g, '').length == 0) {
        valid = false;
        errorText = (errorText.length > 0) ? errorText += '<br>Last-Name is mandatory!' : errorText = 'Last-Name is mandatory!';
    }
    if (!phone || phone.replace(/ /g, '').length == 0) {
        valid = false;
        errorText = (errorText.length > 0) ? errorText += '<br>Phone-Number is mandatory!' : errorText = 'Phone-Number is mandatory!';
    }
    // if (!email || email.replace(/ /g, '').length == 0) {
    //     valid = false;
    //     errorText = (errorText.length > 0) ? errorText += '<br>Email is mandatory!' : errorText = 'Email is mandatory!';
    // }
    if (phone && !(phone.replace(/ /g, '').length == 0)) {
        let phoneExp = /^\d{10}$/;
        if (!(phone.match(phoneExp))) {
            valid = false;
            errorText = (errorText.length > 0) ? errorText += '<br>Phone-Number should be a 10-digit numeric value!' : errorText = 'Phone-Number should be a 10-digit numeric value!!';
        }
    }
    if (email && !(email.replace(/ /g, '').length == 0)) {
        let emailExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!(email.match(emailExp))) {
            valid = false;
            errorText = (errorText.length > 0) ? errorText += '<br>Email is invalid!' : errorText = 'Email is invalid!';
        }
    }

    //If validations passed, Fetch-API call via Observables to add-contact to the DB
    if (valid) {
        const addContact$ = Observable.create(observer => {
            fetch('http://localhost:3000/contacts', {
                method: "POST",
                mode: "cors",
                cache: "no-cache",
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json",
                },
                redirect: "follow",
                referrer: "no-referrer",
                body: JSON.stringify({
                    firstname: fname,
                    lastname: lname,
                    phone: phone,
                    email: email
                }),
            })
                .then((resp) => resp.json())
                .then(data => {
                    observer.next(data);
                    observer.complete();
                })
                .catch(err => observer.error(err));
        });
        //Subscription to the Observable
        addContact$.subscribe(contact => {
            alert('Contact added Successfully!');
            document.getElementById('fname').value = '';
            document.getElementById('lname').value = '';
            document.getElementById('phone').value = '';
            document.getElementById('email').value = '';

            getAllContacts();
            document.getElementById('showAllContacts').style.display = 'block';
            document.getElementById('addSection').style.display = 'none';
            document.getElementById('errMsg').innerHTML = '';

        });
    } else {
        //Display error-message if validations fail
        document.getElementById('errMsg').innerHTML = errorText;
    }

}

//Function to show contact-details when contact is clicked
function showContactDetails(id) {
    //Fetch-API call via Observables to get contact-details based on Contact-ID
    const viewContact$ = Observable.create(observer => {
        fetch('http://localhost:3000/contacts/' + id)
            .then(response => response.json()) // or text() or blob() etc.
            .then(contact => {
                observer.next(contact);
                observer.complete();
            })
            .catch(err => observer.error(err));
    });
    //Subscription to Observable
    viewContact$.subscribe(contact => {
        //Display contact-details in Modal
        const modal = document.getElementById('myModal');
        modal.style.display = "block";
        document.getElementById("frstname").innerHTML = contact.firstname;
        document.getElementById("lstname").innerHTML = contact.lastname;
        document.getElementById("phn").innerHTML = contact.phone;
        document.getElementById("mail").innerHTML = contact.email;
    });
}


